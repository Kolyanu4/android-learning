package com.bignerdranch.android.criminalintent;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;

import java.util.UUID;

public class CrimeActivity extends SingleFragmentActivity {

    private static final String EXTRA_CRIME_ID = "com.bignerdranch.android.criminalintent.crime_id";
    private static final String EXTRA_CRIME_POSITION = "com.bignerdranch.android.criminalintent.crime_position";

    public static Intent newIntent(Context ctx, UUID crimeId, Integer crimePosition) {
        Intent intent = new Intent(ctx, CrimeActivity.class);

        Bundle args = new Bundle();
        args.putSerializable(EXTRA_CRIME_ID, crimeId);
        args.putSerializable(EXTRA_CRIME_POSITION, crimePosition);

        intent.putExtras(args);
        return intent;
    }

    @Override
    protected Fragment createFragment() {
        Bundle args = getIntent().getExtras();

        UUID crimeId = (UUID) args.getSerializable(EXTRA_CRIME_ID);
        return CrimeFragment.newInstance(crimeId, args.getInt(EXTRA_CRIME_POSITION));
    }
}
