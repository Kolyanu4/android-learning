package com.bignerdranch.android.photogalery;

import android.net.Uri;

import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import timber.log.Timber;

public class FlickrFetcher {

    private static final String API_KEY = "7c0bfc616c282d40fa9c1b2909600836";
    private static final String API_ROOT = "https://api.flickr.com/services/rest/";
    // https://api.flickr.com/services/rest/?method=flickr.photos.getRecent&api_key=7c0bfc616c282d40fa9c1b2909600836&format=json&nojsoncallback=1

    public byte[] getUrlBytes(String urlSpec) throws IOException {
        URL url = new URL(urlSpec);
        HttpURLConnection connection = (HttpURLConnection) url.openConnection();
        try {
            ByteArrayOutputStream out = new ByteArrayOutputStream();
            InputStream in = connection.getInputStream();

            if (connection.getResponseCode() != HttpURLConnection.HTTP_OK) {
                throw new IOException(connection.getResponseMessage() + ": with " + urlSpec);
            }

            int bytesRead = 0;
            byte[] buffer = new byte[1024];
            while ((bytesRead = in.read(buffer)) > 0) {
                out.write(buffer, 0, bytesRead);
            }
            out.close();
            return out.toByteArray();

        } finally {
            connection.disconnect();
        }
    }

    public String getUrlString(String urlSpec) throws IOException {
        return new String(getUrlBytes(urlSpec));
    }

    public  List<GalleryItem> fetchItems() {
        List<GalleryItem> items = new ArrayList<>();

        try {
            String url = Uri.parse(API_ROOT).buildUpon().
                    appendQueryParameter("method", "flickr.photos.getRecent").
                    appendQueryParameter("api_key", API_KEY).
                    appendQueryParameter("format", "json").
                    appendQueryParameter("nojsoncallback", "1").
                    appendQueryParameter("extras", "url_s").
                    build().toString();
            String jsonString = getUrlString(url);
            Timber.i("Received JSON: " + jsonString);
            JSONObject jsonBody = new JSONObject(jsonString);

            parseItems(items, jsonBody);
        } catch (JSONException|IOException exc) {
            Timber.e(exc, "Fetch failed");
        }
        return items;
    }

    private void parseItems(List<GalleryItem> items, JSONObject jsonBody)
            throws IOException, JSONException {
        Gson gson = new Gson();

        JSONObject photosJsonObject = jsonBody.getJSONObject("photos");
        JSONArray photoJsonArray = photosJsonObject.getJSONArray("photo");
        GalleryItem[] itemsTemp = gson.fromJson(photoJsonArray.toString(), GalleryItem[].class);

        for (GalleryItem item: itemsTemp){
            if (item.getUrl() == null || item.getUrl().isEmpty()) {
                continue;
            }
            items.add(item);
        }
    }
}
