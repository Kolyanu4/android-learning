package com.bignerdranch.android.photogalery;

import android.os.Bundle;
import android.support.v4.app.Fragment;

import timber.log.Timber;

public class PhotoGalleryActivity extends SingleFragmentActivity {

    @Override
    public Fragment createFragment() {
        return PhotoGalleryFragment.newInstance();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Timber.plant(new Timber.DebugTree());
        super.onCreate(savedInstanceState);
    }
}
