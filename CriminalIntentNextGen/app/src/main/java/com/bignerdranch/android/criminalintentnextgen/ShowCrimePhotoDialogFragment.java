package com.bignerdranch.android.criminalintentnextgen;

import android.app.Dialog;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.ImageView;

import java.io.File;
import java.util.UUID;

public class ShowCrimePhotoDialogFragment extends DialogFragment {

    private static final String ARG_UUID = "uuid";

    public static ShowCrimePhotoDialogFragment newInstance(Crime crime) {
        Bundle args = new Bundle();
        args.putSerializable(ARG_UUID, crime.getId());

        ShowCrimePhotoDialogFragment crimePhotoDialogFragment = new ShowCrimePhotoDialogFragment();
        crimePhotoDialogFragment.setArguments(args);
        return crimePhotoDialogFragment;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        UUID id = (UUID) getArguments().getSerializable(ARG_UUID);
        final CrimeLab crimeLab = CrimeLab.getInstance(getActivity());
        final Crime crime = crimeLab.getCrime(id);

        View view = LayoutInflater.from(getActivity()).inflate(R.layout.crime_photo_dialog, null);
        final ImageView iv = (ImageView) view.findViewById(R.id.crime_photo_detailed);

        ViewTreeObserver observer = iv.getViewTreeObserver();
        observer.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                File photoFile = crimeLab.getPhotoFile(crime);
                Bitmap bitmap = PictureUtils.getScaledBitmap(
                        photoFile.getPath(), iv.getWidth(), iv.getHeight());
                iv.setImageBitmap(bitmap);
            }
        });
        return new AlertDialog.Builder(getActivity()).setPositiveButton("Ok", null).setView(view).create();
    }
}
