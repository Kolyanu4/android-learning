package com.bignerdranch.android.criminalintentnextgen;

import java.util.Date;
import java.util.UUID;

public class Crime {

    private UUID id;
    private String title;
    private boolean isSolved;
    private Date dateCreated;
    private String suspect;

    public Crime() {
        this(UUID.randomUUID());
    }

    public Crime(UUID crimeId) {
        this.id = crimeId;
        this.dateCreated = new Date();
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public boolean isSolved() {
        return isSolved;
    }

    public void setSolved(boolean solved) {
        isSolved = solved;
    }

    public Date getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(Date dateCreated) {
        this.dateCreated = dateCreated;
    }

    public String getSuspect() {
        return suspect;
    }

    public void setSuspect(String suspect) {
        this.suspect = suspect;
    }

    public String getPhotoFilename() {
        return "IMG_" + getId().toString() + ".jpg";
    }
}
