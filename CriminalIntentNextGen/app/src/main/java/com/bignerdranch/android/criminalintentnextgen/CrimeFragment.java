package com.bignerdranch.android.criminalintentnextgen;

import android.Manifest;
import android.app.Activity;
import android.content.ContentResolver;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract.Contacts;
import android.provider.ContactsContract.CommonDataKinds.Phone;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.ShareCompat;
import android.support.v4.content.ContextCompat;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;

import com.bignerdranch.android.criminalintentnextgen.reports.CrimeReport;

import java.io.File;
import java.text.DateFormat;
import java.util.Date;
import java.util.UUID;


public class CrimeFragment extends Fragment {

    private static final String ARG_CRIME_ID = "crime_id";
    private static final String DIALOG_DATE = "DialogDate";
    private static final String PHOTO_DETAILED = "PhotoDetailed";
    private static final int REQUEST_DATE = 0;
    private static final int REQUEST_CONTACT = 1;
    private static final int REQUEST_DIAL = 2;
    private static final int REQUEST_READ_CONTACTS_PERMISSION = 3;
    private static final int REQUEST_PHOTO = 4;

    private Crime crime;
    private File photoFile;
    private EditText titleField;
    private Button dateButton;
    private CheckBox solvedCheckBox;
    private Button suspectBtn;
    private ImageButton photoBtn;
    private ImageView photoView;
    private CrimeListFragment.CrimeUpdateCallback callback;

    public static CrimeFragment newInstance(UUID crimeId) {
        Bundle args = new Bundle();
        args.putSerializable(ARG_CRIME_ID, crimeId);
        CrimeFragment fragment = new CrimeFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);

        UUID crimeId = (UUID) getArguments().getSerializable(ARG_CRIME_ID);
        this.crime = CrimeLab.getInstance(getActivity()).getCrime(crimeId);
        photoFile = CrimeLab.getInstance(getActivity()).getPhotoFile(crime);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_crime, container, false);

        this.titleField = (EditText) v.findViewById(R.id.crime_title);
        this.titleField.setText(this.crime.getTitle());
        this.titleField.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                crime.setTitle(charSequence.toString());
                updateCrime();
            }

            @Override
            public void afterTextChanged(Editable editable) {
            }
        });

        this.dateButton = (Button) v.findViewById(R.id.crime_date);
        this.updateDateText();
        this.dateButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FragmentManager fm = getFragmentManager();
                DatePickerFragment dialog = DatePickerFragment.newInstance(crime.getDateCreated());
                dialog.setTargetFragment(CrimeFragment.this, REQUEST_DATE);
                dialog.show(fm, DIALOG_DATE);
            }
        });

        this.solvedCheckBox = (CheckBox) v.findViewById(R.id.crime_solved);
        this.solvedCheckBox.setChecked(this.crime.isSolved());
        this.solvedCheckBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                crime.setSolved(b);
                updateCrime();
            }
        });

        Button sendReportBtn = (Button) v.findViewById(R.id.crime_report_btn);
        sendReportBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = ShareCompat.IntentBuilder.from(getActivity())
                        .setSubject(getString(R.string.crime_report_subject))
                        .setText(CrimeReport.getReport(getResources(), crime))
                        .setType("text/plain")
                        .createChooserIntent();
                startActivity(i);
            }
        });

        final Intent pickContact = new Intent(Intent.ACTION_PICK, Contacts.CONTENT_URI);
        PackageManager packageManager = getActivity().getPackageManager();

        suspectBtn = (Button) v.findViewById(R.id.crime_suspect_btn);
        suspectBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivityForResult(pickContact, REQUEST_CONTACT);
            }
        });

        if (packageManager.resolveActivity(pickContact,
                PackageManager.MATCH_DEFAULT_ONLY) == null) {
            suspectBtn.setEnabled(false);
        }

        if (crime.getSuspect() != null) {
            suspectBtn.setText(crime.getSuspect());
        }

        Button callBtn = (Button) v.findViewById(R.id.crime_call_btn);
        callBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (ContextCompat.checkSelfPermission(getActivity(),
                        Manifest.permission.READ_CONTACTS)
                        != PackageManager.PERMISSION_GRANTED) {
                    requestPermissions(new String[]{Manifest.permission.READ_CONTACTS},
                            REQUEST_READ_CONTACTS_PERMISSION);
                } else {
                    startActivityForResult(pickContact, REQUEST_DIAL);
                }
            }
        });

        final Intent captureImage = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        photoBtn = (ImageButton) v.findViewById(R.id.crime_camera_btn);
        boolean canTakePhoto = photoFile != null &&
                captureImage.resolveActivity(packageManager) != null;
        photoBtn.setEnabled(canTakePhoto);

        if (canTakePhoto) {
            Uri uri = Uri.fromFile(photoFile);
            captureImage.putExtra(MediaStore.EXTRA_OUTPUT, uri);
        }
        photoBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivityForResult(captureImage, REQUEST_PHOTO);
            }
        });

        photoView = (ImageView) v.findViewById(R.id.crime_photo_view);
        ViewTreeObserver observer = photoView.getViewTreeObserver();
        observer.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                updatePhotoView();
            }
        });
        photoView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FragmentManager fm = getFragmentManager();
                ShowCrimePhotoDialogFragment dialog = ShowCrimePhotoDialogFragment.newInstance(crime);
                dialog.show(fm, PHOTO_DETAILED);
            }
        });

        return v;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode != Activity.RESULT_OK) {
            return;
        }

        switch (requestCode) {
            case REQUEST_DATE: {
                Date date = (Date) data.getSerializableExtra(DatePickerFragment.EXTRA_DATE);
                this.crime.setDateCreated(date);
                updateDateText();
                updateCrime();
                return;
            }
            case REQUEST_CONTACT: {
                if (data != null) {
                    Uri contactUri = data.getData();
                    String[] queryFields = new String[]{Contacts.DISPLAY_NAME};
                    Cursor c = getActivity().getContentResolver().query(contactUri, queryFields, null, null, null);
                    try {
                        if (c.getCount() == 0) {
                            return;
                        }
                        c.moveToFirst();
                        String suspect = c.getString(0);
                        crime.setSuspect(suspect);
                        suspectBtn.setText(suspect);
                        updateCrime();
                    } finally {
                        c.close();
                    }
                }
                return;
            }
            case REQUEST_DIAL: {
                if (data != null) {
                    String number = null;

                    Uri contactUri = data.getData();
                    String[] queryFields = new String[]{Contacts._ID};
                    ContentResolver cr = getActivity().getContentResolver();
                    Cursor c = cr.query(contactUri, queryFields, null, null, null);
                    try {
                        if (c.getCount() == 0) {
                            return;
                        }
                        c.moveToFirst();
                        String contactId = c.getString(c.getColumnIndex(Contacts._ID));
                        Cursor phones = cr.query(Phone.CONTENT_URI, null, Phone.CONTACT_ID + " = ?", new String[]{contactId}, null);

                        try {
                            if (phones.getCount() == 0) {
                                return;
                            }
                            phones.moveToFirst();
                            number = phones.getString(phones.getColumnIndex(Phone.NUMBER));
                        } finally {
                            phones.close();
                        }

                    } finally {
                        c.close();
                    }

                    if (number != null) {
                        Intent i = new Intent(Intent.ACTION_DIAL);
                        i.setData(Uri.parse("tel:" + number));
                        startActivity(i);
                    }
                }
                return;
            }
            case REQUEST_PHOTO: {
                updatePhotoView();
                return;
            }
        }
    }

    private void updateDateText() {
        this.dateButton.setText(DateFormat.getDateInstance(DateFormat.FULL).format(crime.getDateCreated()));
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.crime_fragment, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.remove_crime_menu:
                CrimeLab.getInstance(getActivity()).removeCrime(this.crime);
                getActivity().finish();
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        getActivity().setResult(Activity.RESULT_OK, null);
        CrimeLab.getInstance(getActivity()).updateCrime(this.crime);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case REQUEST_READ_CONTACTS_PERMISSION:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    startActivityForResult(new Intent(Intent.ACTION_PICK, Contacts.CONTENT_URI), REQUEST_DIAL);
                }
            default:
                return;
        }
    }

    private void updatePhotoView() {
        if (photoFile == null || !photoFile.exists()) {
            photoView.setImageDrawable(null);
        } else {
            Bitmap bitmap = PictureUtils.getScaledBitmap(
                    photoFile.getPath(), photoView.getWidth(), photoView.getHeight());
            photoView.setImageBitmap(bitmap);
        }
    }

    public void setCallback(CrimeListFragment.CrimeUpdateCallback callback) {
        this.callback = callback;
    }
    private void updateCrime() {
        CrimeLab.getInstance(getActivity()).updateCrime(crime);
        if (this.callback != null) {
            this.callback.crimeUpdate();
        }
    }
}
