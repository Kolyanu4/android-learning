package com.bignerdranch.android.criminalintentnextgen.reports;

import android.content.res.Resources;
import android.text.format.DateFormat;

import com.bignerdranch.android.criminalintentnextgen.Crime;
import com.bignerdranch.android.criminalintentnextgen.R;

public class CrimeReport {

    public static String getReport(Resources res, Crime crime) {
        String solvedString;
        if (crime.isSolved()) {
            solvedString = res.getString(R.string.crime_report_solved);
        } else {
            solvedString = res.getString(R.string.crime_report_unsolved);
        }
        String dateFormat = "EEE, MMM dd";
        String dateString = DateFormat.format(dateFormat, crime.getDateCreated()).toString();
        String suspect = crime.getSuspect();
        if (suspect == null) {
            suspect = res.getString(R.string.crime_report_no_suspect);
        } else {
            suspect = res.getString(R.string.crime_report_suspect, suspect);
        }
        return res.getString(R.string.crime_report, crime.getTitle(), dateString, solvedString, suspect);
    }
}
