package com.bignerdranch.android.criminalintentnextgen;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;


public class CrimeListFragment extends Fragment {

    private static final int CRIME_DETAIL_ACTIVITY_CODE = 200;
    private static final String SAVED_SUBTITLE_VISIBLE = "subtitle";

    private boolean isSubtitleVisible;

    private CrimeAdapter crimeAdapter;

    public interface CrimeClickCallback {
        void crimeClick(Crime crime);
    }
    public interface CrimeUpdateCallback {
        void crimeUpdate();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_crime_list, container, false);

        crimeAdapter = new CrimeAdapter(
                CrimeLab.getInstance(getActivity()).getCrimes(),
                (CrimeListActivity) getActivity()
        );

        RecyclerView recyclerView = (RecyclerView) view.findViewById(R.id.crime_recycler_view);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerView.setAdapter(crimeAdapter);

        crimeAdapter.setCallback(new CrimeClickCallback() {
            @Override
            public void crimeClick(Crime crime) {
                crimeClickCallback(crime, CRIME_DETAIL_ACTIVITY_CODE);
            }
        });

        if (savedInstanceState != null) {
            isSubtitleVisible = savedInstanceState.getBoolean(SAVED_SUBTITLE_VISIBLE);
        }
        updateSubtitle();

        return view;
    }

    public void crimeClickCallback(Crime crime, int result_code) {
        if (getActivity().findViewById(R.id.detail_fragment_container) == null) {
            Intent intent = CrimePagerActivity.newIntent(getActivity(), crime.getId());
            startActivityForResult(intent, result_code);
        } else {
            CrimeFragment newDetail = CrimeFragment.newInstance(crime.getId());
            newDetail.setCallback(new CrimeUpdateCallback() {
                @Override
                public void crimeUpdate() {
                    updateUI();
                }
            });
            getActivity().getSupportFragmentManager().beginTransaction()
                    .replace(R.id.detail_fragment_container, newDetail)
                    .commit();
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == CRIME_DETAIL_ACTIVITY_CODE) {
                crimeAdapter.setCrimes(CrimeLab.getInstance(getActivity()).getCrimes());
                updateUI();
            }
        }
    }

    @Override
    public void onCreateOptionsMenu (Menu menu, MenuInflater inflater){
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.fragment_crime_list, menu);

        MenuItem subtitleItem = menu.findItem(R.id.menu_item_show_subtitle);
        if (isSubtitleVisible) {
            subtitleItem.setTitle(R.string.hide_subtitle);
        } else {
            subtitleItem.setTitle(R.string.show_subtitle);
        }
    }

    public void updateUI() {
        crimeAdapter.setCrimes(CrimeLab.getInstance(getActivity()).getCrimes());
        crimeAdapter.notifyDataSetChanged();
        updateSubtitle();
    }

    @Override
    public boolean onOptionsItemSelected (MenuItem item){
        switch (item.getItemId()) {
            case R.id.menu_item_new_crime:
                Crime crime = new Crime();
                CrimeLab.getInstance(getActivity()).addCrime(crime);
                updateUI();
                crimeClickCallback(crime, CRIME_DETAIL_ACTIVITY_CODE);
                return true;

            case R.id.menu_item_show_subtitle:
                isSubtitleVisible = !isSubtitleVisible;
                getActivity().invalidateOptionsMenu();
                updateSubtitle();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void updateSubtitle() {
        CrimeLab crimeLab = CrimeLab.getInstance(getActivity());
        int crimeCount = crimeLab.getCrimes().size();
        String subtitle = getResources().getQuantityString(
                R.plurals.subtitle_format, crimeCount, crimeCount
        );

        if (!isSubtitleVisible) {
            subtitle = null;
        }

        AppCompatActivity activity = (AppCompatActivity) getActivity();
        activity.getSupportActionBar().setSubtitle(subtitle);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putBoolean(SAVED_SUBTITLE_VISIBLE, isSubtitleVisible);
    }

}
