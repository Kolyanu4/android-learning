package com.bignerdranch.android.criminalintentnextgen;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

import java.util.List;

public class CrimeAdapter extends RecyclerView.Adapter<CrimeAdapter.CrimeHolder> {
    private List<Crime> crimes;
    private CrimeListActivity activity;
    private CrimeListFragment.CrimeClickCallback callback;

    public CrimeAdapter(List<Crime> crimes, CrimeListActivity activity) {
        this.crimes = crimes;
        this.activity = activity;
    }

    @Override
    public CrimeHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(this.activity);
        View view = layoutInflater.inflate(R.layout.list_item_crime, parent, false);
        return new CrimeHolder(view);
    }

    @Override
    public void onBindViewHolder(CrimeHolder holder, int position) {
        final Crime crime = this.crimes.get(position);
        holder.bindCrime(crime);
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                callback.crimeClick(crime);
            }
        });
    }

    @Override
    public int getItemCount() {
        return this.crimes.size();
    }

    public void setCallback(CrimeListFragment.CrimeClickCallback callback) {
        this.callback = callback;
    }

    class CrimeHolder extends RecyclerView.ViewHolder {

        public TextView titleTextView;
        public TextView dateTextView;
        public CheckBox solvedCheckBox;

        public CrimeHolder(View view) {
            super(view);
            this.titleTextView = (TextView) view.findViewById(R.id.list_item_crime_title_text_view);
            this.dateTextView = (TextView) view.findViewById(R.id.list_item_crime_date_text_view);
            this.solvedCheckBox = (CheckBox) view.findViewById(R.id.list_item_crime_solved_check_box);
        }

        public void bindCrime(final Crime crime) {
            this.titleTextView.setText(crime.getTitle());
            this.dateTextView.setText(crime.getDateCreated().toString());
            this.solvedCheckBox.setChecked(crime.isSolved());
        }
    }

    public void setCrimes(List<Crime> crimes) {
        this.crimes = crimes;
    }
}
