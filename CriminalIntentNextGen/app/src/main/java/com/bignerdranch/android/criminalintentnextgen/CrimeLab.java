package com.bignerdranch.android.criminalintentnextgen;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Environment;

import com.bignerdranch.android.criminalintentnextgen.database.CrimeBaseHelper;
import com.bignerdranch.android.criminalintentnextgen.database.CrimeCursorWrapper;
import com.bignerdranch.android.criminalintentnextgen.database.CrimeDbSchema;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class CrimeLab {

    private static CrimeLab crimeLab;
    private Context context;
    private SQLiteDatabase database;

    public static CrimeLab getInstance(Context ctx) {
        if (crimeLab == null) {
            crimeLab = new CrimeLab(ctx);
        }
        return crimeLab;
    }

    private CrimeLab(Context ctx) {
        context = ctx.getApplicationContext();
        database = new CrimeBaseHelper(context).getWritableDatabase();
    }

    public List<Crime> getCrimes() {
        List<Crime> crimes = new ArrayList<>();
        CrimeCursorWrapper cursor = queryCrimes(null, null);
        try {
            cursor.moveToFirst();
            while (!cursor.isAfterLast()) {
                crimes.add(cursor.getCrime());
                cursor.moveToNext();
            }
        } finally {
            cursor.close();
        }
        return crimes;
    }

    public Crime getCrime(UUID crimeId) {
        CrimeCursorWrapper cursor = queryCrimes(
            CrimeDbSchema.CrimeTable.Cols.UUID + " = ?",
            new String[] { crimeId.toString() }
        );

        try {
            if (cursor.getCount() == 0) {
                return null;
            }
            cursor.moveToFirst();
            return cursor.getCrime();
        } finally {
            cursor.close();
        }
    }

    public void addCrime(Crime crime) {
        ContentValues values = CrimeLab.getContentValues(crime);
        database.insert(CrimeDbSchema.CrimeTable.NAME, null, values);
    }

    public void removeCrime(Crime crime) {
        String crimeID = crime.getId().toString();

        database.delete(
            CrimeDbSchema.CrimeTable.NAME,
            CrimeDbSchema.CrimeTable.Cols.UUID + " = ?",
            new String[] { crimeID }
        );
    }

    private static ContentValues getContentValues(Crime crime) {
        ContentValues values = new ContentValues();
        values.put(CrimeDbSchema.CrimeTable.Cols.UUID, crime.getId().toString());
        values.put(CrimeDbSchema.CrimeTable.Cols.TITLE, crime.getTitle());
        values.put(CrimeDbSchema.CrimeTable.Cols.DATE, crime.getDateCreated().getTime());
        values.put(CrimeDbSchema.CrimeTable.Cols.SOLVED, crime.isSolved() ? 1 : 0);
        values.put(CrimeDbSchema.CrimeTable.Cols.SUSPECT, crime.getSuspect());
        return values;
    }

    public void updateCrime(Crime crime) {
        String uuidString = crime.getId().toString();
        ContentValues values = CrimeLab.getContentValues(crime);

        database.update(
            CrimeDbSchema.CrimeTable.NAME,
            values,
            CrimeDbSchema.CrimeTable.Cols.UUID + " = ?",
            new String[] { uuidString }
        );
    }

    private CrimeCursorWrapper queryCrimes(String whereClause, String[] whereArgs) {
        Cursor cursor = database.query(
                CrimeDbSchema.CrimeTable.NAME,
                null,
                whereClause,
                whereArgs,
                null,  // groupBy
                null,  // having
                null  // orderBy
        );
        return new CrimeCursorWrapper(cursor);
    }

    public File getPhotoFile(Crime crime) {
        File externalFilesDir = context.getExternalFilesDir(Environment.DIRECTORY_PICTURES);

        if (externalFilesDir == null) {
            return null;
        }

        return new File(externalFilesDir, crime.getPhotoFilename());
    }
}
