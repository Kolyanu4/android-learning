package com.bignerdranch.android.beatbox;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import java.util.List;


public class SoundAdapter extends RecyclerView.Adapter<SoundAdapter.SoundHolder> {

    private BeatBoxActivity activity;
    private List<Sound> sounds;
    private BeatBoxFragment.Callback callback;

    public SoundAdapter(BeatBoxActivity activity, List<Sound> sounds) {
        this.activity = activity;
        this.sounds = sounds;
    }

    @Override
    public int getItemCount() {
        return sounds.size();
    }

    @Override
    public void onBindViewHolder(SoundHolder holder, int position) {
        Sound sound = sounds.get(position);
        holder.bindSound(sound);
    }

    @Override
    public SoundHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new SoundHolder(LayoutInflater.from(this.activity), parent);
    }

    class SoundHolder extends RecyclerView.ViewHolder {

        private Button soundBtn;
        private Sound sound;

        public SoundHolder(LayoutInflater inflater, ViewGroup container) {
            super(inflater.inflate(R.layout.list_item_sound, container, false));

            soundBtn = (Button) itemView.findViewById(R.id.list_item_sound_button);
        }

        public void bindSound(final Sound sound) {
            this.sound = sound;
            soundBtn.setText(sound.getName());
            soundBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    callback.btnClicked(sound);
                }
            });
        }
    }

    public void setCallback(BeatBoxFragment.Callback callback) {
        this.callback = callback;
    }
}
