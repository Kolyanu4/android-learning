package com.bignerdranch.android.geoquiz;

public class Questioner {

    private int currentIndex = 0;
    private Question[] questionsList = new Question[] {
            new Question(R.string.question_oceans, true),
            new Question(R.string.question_mideast, false),
            new Question(R.string.question_africa, false),
            new Question(R.string.question_americas, true),
            new Question(R.string.question_asia, true),
    };

    public Questioner() {}

    public Questioner(int startIndex) {
        currentIndex = startIndex;
    }

    private Question getQuestionByIndex(int index) {
        return questionsList[index];
    }

    public void updateQuestionIndexToNext() {
        currentIndex = (currentIndex + 1)  % questionsList.length;
        return;
    }

    public void updateQuestionIndexToPrev() {
        currentIndex = (questionsList.length + (currentIndex - 1))  % questionsList.length;
        return;
    }

    public Question getCurrentQuestion() {
        return getQuestionByIndex(currentIndex);
    }

    public boolean checkAnswer(boolean answer) {
        return answer == getCurrentQuestion().isAnswerTrue();
    }

    public int getCurrentIndex() {
        return currentIndex;
    }

    public void setCurrentIndex(int currentIndex) {
        this.currentIndex = currentIndex;
    }
}
