package com.bignerdranch.android.geoquiz;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;


public class QuizActivity extends AppCompatActivity {

    private static final String TAG = "QuizActivity";
    private static final String START_INDEX_KEY = "start_index";
    private static final String IS_CHEATER = "is_cheater";
    private static final int REQUEST_CODE_CHEAT = 0;

    private Questioner questioner;
    private TextView questionTextView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d(TAG, "onCreate called");

        setContentView(R.layout.activity_quiz);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        questioner = new Questioner();

        if (savedInstanceState != null) {
            questioner.setCurrentIndex(savedInstanceState.getInt(START_INDEX_KEY, 0));
            questioner.getCurrentQuestion().setCheated(savedInstanceState.getBoolean(IS_CHEATER, false));
        }

        View.OnClickListener nextBtnListener = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                questioner.updateQuestionIndexToNext();
                updateQuestionView();
            }
        };

        View.OnClickListener prevBtnListener = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                questioner.updateQuestionIndexToPrev();
                updateQuestionView();
            }
        };

        questionTextView = (TextView) findViewById(R.id.question_text_view);
        questionTextView.setOnClickListener(nextBtnListener);
        updateQuestionView();

        findViewById(R.id.true_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                checkAnswer(true);
            }
        });

        findViewById(R.id.false_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                checkAnswer(false);
            }
        });

        findViewById(R.id.next_button).setOnClickListener(nextBtnListener);
        findViewById(R.id.next_img_button).setOnClickListener(nextBtnListener);
        findViewById(R.id.previous_button).setOnClickListener(prevBtnListener);
        findViewById(R.id.previous_img_button).setOnClickListener(prevBtnListener);

        Button cheatButton = (Button) findViewById(R.id.cheat_button);
        cheatButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent cheatIntent = CheatActivity.newIntent(
                        QuizActivity.this,
                        questioner.getCurrentQuestion().isAnswerTrue()
                );
                startActivityForResult(cheatIntent, REQUEST_CODE_CHEAT);
            }
        });

    }

    private void updateQuestionView() {
        Question question = questioner.getCurrentQuestion();
        questionTextView.setText(question.getTextResID());
    }

    private void checkAnswer(boolean answer) {
        int messageResId;

        if (questioner.getCurrentQuestion().isCheated()) {
            messageResId = R.string.judgment_toast;
        } else {
            if (questioner.checkAnswer(answer)) {
                messageResId = R.string.correct_toast;
            } else {
                messageResId = R.string.incorrect_toast;
            }
        }
        Toast.makeText(this, messageResId, Toast.LENGTH_SHORT).show();
    }

    @Override
    protected void onStart() {
        super.onStart();
        Log.d(TAG, "onStart");
    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.d(TAG, "onStop");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.d(TAG, "onDestroy");
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.d(TAG, "onPause");
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.d(TAG, "onResume");
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        Log.d(TAG, "onSaveInstanceState");
        outState.putInt(START_INDEX_KEY, questioner.getCurrentIndex());
        outState.putBoolean(IS_CHEATER, questioner.getCurrentQuestion().isCheated());
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode != Activity.RESULT_OK) {
            return;
        }

        if (requestCode == QuizActivity.REQUEST_CODE_CHEAT) {
            if (data == null) {
                return;
            }
            questioner.getCurrentQuestion().setCheated(CheatActivity.wasAnswerShown(data));
        }
    }
}
