package com.bignerdranch.android.nerdlauncher;

import android.content.Context;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.graphics.drawable.Drawable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;


public class ActivityAdapter extends RecyclerView.Adapter<ActivityAdapter.ActivityHolder> {

    private final List<ResolveInfo> activities;
    private final Context ctx;
    private NerdLauncherFragment.Callback callback;

    public ActivityAdapter(List<ResolveInfo> activities, Context ctx) {
        this.activities = activities;
        this.ctx = ctx;
    }

    @Override
    public int getItemCount() {
        return this.activities.size();
    }

    @Override
    public void onBindViewHolder(ActivityHolder holder, int position) {
        ResolveInfo resolveInfo = this.activities.get(position);
        holder.bindActivity(resolveInfo);
    }

    @Override
    public ActivityHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(this.ctx);
        View view = layoutInflater.inflate(R.layout.item_list, parent, false);
        return new ActivityHolder(view, ctx);
    }

    public void setCallback(NerdLauncherFragment.Callback callback) {
        this.callback = callback;
    }

    class ActivityHolder extends RecyclerView.ViewHolder {

        private ResolveInfo resolveInfo;
        private TextView textView;
        private ImageView imageView;
        private Context ctx;

        public ActivityHolder(View view, Context ctx) {
            super(view);
            this.ctx = ctx;
            textView = (TextView) view.findViewById(R.id.activity_name);
            imageView = (ImageView) view.findViewById(R.id.activity_icon);
        }

        public void bindActivity(final ResolveInfo resolveInfo) {
            this.resolveInfo = resolveInfo;
            PackageManager pm = this.ctx.getPackageManager();
            String appName = this.resolveInfo.loadLabel(pm).toString();
            textView.setText(appName);
            textView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    callback.onActivityClicked(resolveInfo);
                }
            });
            Drawable icon = this.resolveInfo.loadIcon(pm);
            imageView.setImageDrawable(icon);
        }
    }

}
